import argparse
import numpy

import tensors
import tensors.tools


def calculate_sine_integral(accuracy):

    def introduce(segment_length):
        print("Integrating sinc from 0 to %.1e..." % segment_length)
        print("The result should be close to half Pi.")
        print("The expected residue is %.1e." % accuracy)

    def integrate(segment_length, grid_size, sinc_accuracy):

        @tensors.tools.tensorized(accuracy=sinc_accuracy)
        def sinc(x):
            return numpy.sin(x) / x

        grid = tensors.Vector.range(grid_size)
        print("%d integration points used for approximation." % grid.length)
        grid_step = segment_length / grid.length
        x = (grid + 0.5) * grid_step
        y = sinc(x)
        return y.sum() * grid_step

    def verify(result):
        half_pi = numpy.pi / 2
        residual = abs(result - half_pi)
        print("Obtained: %.15f." % result)
        print("Expected: %.15f." % half_pi)
        print("Residual: %.1e." % residual)

    segment_length = 6 / accuracy
    introduce(segment_length)
    grid_size = int(3 / accuracy**2)
    sinc_accuracy = pow(accuracy / 3, 1.5) / numpy.sqrt(numpy.pi)
    try:
        verify(integrate(segment_length, grid_size, sinc_accuracy))
    except RuntimeError:
        print("Sorry, cannot achieve the requested accuracy.")


if __name__ == "__main__":
    description = "calculates the sine integral with the given accuracy"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("accuracy",
        type=float,
        nargs="?",
        default=1e-3,
        help="accuracy of the approximation (default is 1e-3)")
    args = parser.parse_args()
    calculate_sine_integral(args.accuracy)
