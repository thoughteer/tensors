.. Tensors documentation master file, created by
   sphinx-quickstart on Fri Nov 14 14:02:40 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Tensors
===================================

A Python 3.x library for working with low-rank tensor approximations.

.. toctree::
    :maxdepth: 2

    api
