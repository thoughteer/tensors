API
=================

.. automodule:: tensors.tensor
    :show-inheritance:
    :special-members:
    :members:

.. automodule:: tensors.scalar
    :show-inheritance:
    :special-members:
    :members:

.. automodule:: tensors.vector
    :show-inheritance:
    :special-members:
    :members:

.. automodule:: tensors.matrix
    :show-inheritance:
    :special-members:
    :members:

.. automodule:: tensors.tools
    :members:
