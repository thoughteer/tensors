# Tensors

A Python 3.x library for working with low-rank tensor approximations.

---

## License

**Tensors** is released under the MIT license.

---

## Installation

Since **Tensors** is compatible with [pip](https://pypi.python.org/pypi/pip),
installation is as simple as
```console
$ pip install tensors
```

---

## Usage

Check out the `examples` directory for usage examples, e.g. run
```console
$ python examples/sine_integral.py
Integrating sinc from 0 to 6.0e+03...
The result should be close to half Pi.
The expected residue is 1.0e-03.
4194304 integration points used for approximation.
Obtained: 1.570645686750360.
Expected: 1.570796326794897.
Residual: 1.5e-04.
```
to calculate the sine integral numerically.

For more detailed information, refer to the complete [API reference
documentation](http://tensors.readthedocs.org/api.html).

---

## Testing

You can run unit tests using [nose](https://nose.readthedocs.org/en/latest/):
```console
$ nosetests -v
```
