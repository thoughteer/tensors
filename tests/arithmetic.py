import nose.tools
import numpy

import tensors
import tests
import tests.data


def add(left, right):
    return left + right


@tests.parameterized
def test_tensor___add___number(left, right, collection):
    result = add(left, right)
    assert result.shape == left.shape
    assert numpy.allclose(result.expand(), right + left.expand())
    collection.append(result)


@tests.parameterized
def test_tensor___add___tensor(left, right, collection):
    if left.shape != right.shape:
        nose.tools.assert_raises(AssertionError, add, left, right)
        return
    result = add(left, right)
    assert result.shape == left.shape
    assert numpy.allclose(result.expand(), left.expand() + right.expand())
    collection.append(result)


def multiply(left, right):
    return left * right


@tests.parameterized
def test_tensor___mul___number(left, right, collection):
    result = multiply(left, right)
    assert result.shape == left.shape
    assert numpy.allclose(result.expand(), right * left.expand())
    collection.append(result)


@tests.parameterized
def test_tensor___mul___tensor(left, right, collection):
    if left.shape != right.shape:
        nose.tools.assert_raises(AssertionError, multiply, left, right)
        return
    result = multiply(left, right)
    assert result.shape == left.shape
    assert numpy.allclose(result.expand(), left.expand() * right.expand())
    collection.append(result)


@tests.parameterized
def test_tensor___neg__(tensor, collection):
    result = -tensor
    assert result.shape == tensor.shape
    assert numpy.allclose(result.expand(), -tensor.expand())
    collection.append(result)


def subtract(left, right):
    return left - right


@tests.parameterized
def test_tensor___sub___number(left, right, collection):
    result = subtract(left, right)
    assert result.shape == left.shape
    assert numpy.allclose(result.expand(), -right + left.expand())
    collection.append(result)


@tests.parameterized
def test_tensor___sub___tensor(left, right, collection):
    if left.shape != right.shape:
        nose.tools.assert_raises(AssertionError, subtract, left, right)
        return
    result = subtract(left, right)
    assert result.shape == left.shape
    assert numpy.allclose(result.expand(), left.expand() - right.expand())
    collection.append(result)


def divide(left, right):
    return left / right


@tests.parameterized
def test_tensor___truediv__(left, right, collection):
    if right == 0:
        return
    result = divide(left, right)
    assert result.shape == left.shape
    assert numpy.allclose(result.expand(), left.expand() * (1 / right))
    collection.append(result)


@tests.parameterized
def test_tensor_round(tensor, accuracy, collection):
    try:
        result = tensor.round(accuracy)
    except:
        print(tensor.expand())
        print(tensor._qtt)
        import tt
        print(tt.tensor.to_list(tensor._qtt))
    assert result.shape == tensor.shape
    error = numpy.linalg.norm(result.expand() - tensor.expand())
    assert error <= accuracy * numpy.linalg.norm(tensor.expand())
    collection.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_tensor_round_fail(tensor, accuracy):
    tensor.round(accuracy)


@tests.parameterized
def test_tensor_sum(tensor):
    result = tensor.sum()
    assert numpy.isclose(result, tensor.expand().sum())


def generate_tests():
    numbers = tests.data.numbers[:]
    scalars = tests.data.scalars[:]
    vectors = tests.data.vectors[:]
    matrices = tests.data.matrices[:]
    yield from test_tensor___add___number(scalars, numbers,
        collection=tests.data.scalars)
    yield from test_tensor___add___number(scalars, scalars,
        collection=tests.data.scalars)
    yield from test_tensor___add___number(vectors, numbers,
        collection=tests.data.vectors)
    yield from test_tensor___add___number(vectors, scalars,
        collection=tests.data.vectors)
    yield from test_tensor___add___number(matrices, numbers,
        collection=tests.data.matrices)
    yield from test_tensor___add___number(matrices, scalars,
        collection=tests.data.matrices)
    yield from test_tensor___add___tensor(scalars, scalars,
        collection=tests.data.scalars)
    yield from test_tensor___add___tensor(vectors, vectors,
        collection=tests.data.vectors)
    yield from test_tensor___add___tensor(matrices, matrices,
        collection=tests.data.matrices)
    yield from test_tensor___mul___number(scalars, numbers,
        collection=tests.data.scalars)
    yield from test_tensor___mul___number(scalars, scalars,
        collection=tests.data.scalars)
    yield from test_tensor___mul___number(vectors, numbers,
        collection=tests.data.vectors)
    yield from test_tensor___mul___number(vectors, scalars,
        collection=tests.data.vectors)
    yield from test_tensor___mul___number(matrices, numbers,
        collection=tests.data.matrices)
    yield from test_tensor___mul___number(matrices, scalars,
        collection=tests.data.matrices)
    yield from test_tensor___mul___tensor(scalars, scalars,
        collection=tests.data.scalars)
    yield from test_tensor___mul___tensor(vectors, vectors,
        collection=tests.data.vectors)
    yield from test_tensor___mul___tensor(matrices, matrices,
        collection=tests.data.matrices)
    yield from test_tensor___neg__(scalars,
        collection=tests.data.scalars)
    yield from test_tensor___neg__(vectors,
        collection=tests.data.vectors)
    yield from test_tensor___neg__(matrices,
        collection=tests.data.matrices)
    yield from test_tensor___sub___number(scalars, numbers,
        collection=tests.data.scalars)
    yield from test_tensor___sub___number(scalars, scalars,
        collection=tests.data.scalars)
    yield from test_tensor___sub___number(vectors, numbers,
        collection=tests.data.vectors)
    yield from test_tensor___sub___number(vectors, scalars,
        collection=tests.data.vectors)
    yield from test_tensor___sub___number(matrices, numbers,
        collection=tests.data.matrices)
    yield from test_tensor___sub___number(matrices, scalars,
        collection=tests.data.matrices)
    yield from test_tensor___sub___tensor(scalars, scalars,
        collection=tests.data.scalars)
    yield from test_tensor___sub___tensor(vectors, vectors,
        collection=tests.data.vectors)
    yield from test_tensor___sub___tensor(matrices, matrices,
        collection=tests.data.matrices)
    yield from test_tensor___truediv__(scalars, numbers,
        collection=tests.data.scalars)
    yield from test_tensor___truediv__(scalars, scalars,
        collection=tests.data.scalars)
    yield from test_tensor___truediv__(vectors, numbers,
        collection=tests.data.vectors)
    yield from test_tensor___truediv__(vectors, scalars,
        collection=tests.data.vectors)
    yield from test_tensor___truediv__(matrices, numbers,
        collection=tests.data.matrices)
    yield from test_tensor___truediv__(matrices, scalars,
        collection=tests.data.matrices)
    yield from test_tensor_round(scalars, tests.data.valid_accuracies,
        collection=tests.data.scalars)
    yield from test_tensor_round_fail(scalars, tests.data.invalid_accuracies)
    yield from test_tensor_round(vectors, tests.data.valid_accuracies,
        collection=tests.data.vectors)
    yield from test_tensor_round_fail(vectors, tests.data.invalid_accuracies)
    yield from test_tensor_round(matrices, tests.data.valid_accuracies,
        collection=tests.data.matrices)
    yield from test_tensor_round_fail(matrices, tests.data.invalid_accuracies)
    yield from test_tensor_sum(scalars)
    yield from test_tensor_sum(vectors)
    yield from test_tensor_sum(matrices)
