import itertools

import tests.data


def call(function, args, kwargs={}):

    def wrapper(*args):
        return function(*args, **kwargs)

    wrapper.description = "%s%s" % (function.__name__, args)
    return (wrapper,) + args


def deferred(function):

    def verbose_function(*args):
        return call(function, args)

    return verbose_function


def parameterized(function):

    def parameterized_function(*argsets, **kwargs):
        return (call(function, args, kwargs) for args in
            itertools.product(*argsets))

    return parameterized_function


def deduplicate_collection(collection):
    keys = [hash(t.__repr__()) for t in collection]
    return list(dict(zip(keys, collection)).values())


def deduplicate_collections():
    tests.data.scalars = deduplicate_collection(tests.data.scalars)
    tests.data.vectors = deduplicate_collection(tests.data.vectors)
    tests.data.matrices = deduplicate_collection(tests.data.matrices)


def generate_initial_data():

    import tests.construction

    yield from tests.construction.generate_tests()


def generate_operations():

    import tests.approximation
    import tests.arithmetic
    import tests.conversion
    import tests.matrix
    import tests.vector

    modules = [
        tests.conversion,
        tests.arithmetic,
        tests.approximation,
        tests.vector,
        tests.matrix
    ]
    for module in modules:
        deduplicate_collections()
        yield from module.generate_tests()


def generate_tests():
    yield from generate_initial_data()
    for i in range(2):
        yield from generate_operations()
