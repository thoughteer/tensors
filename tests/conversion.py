import nose.tools
import numpy

import tensors
import tests
import tests.data


@tests.parameterized
def test_scalar_from_number(other):
    result = tensors.Scalar(other)
    assert result.shape == ()
    assert numpy.allclose(result.expand(), other)
    tests.data.scalars.append(result)


@tests.parameterized
def test_scalar_from_scalar(other):
    result = tensors.Scalar(other)
    assert result.shape == ()
    assert numpy.allclose(result.expand(), other.expand())


@tests.parameterized
@nose.tools.raises(TypeError)
def test_scalar_fail(other):
    tensors.Scalar(other)


@tests.parameterized
def test_vector_from_number(other):
    result = tensors.Vector(other)
    assert result.length == 1
    assert result.shape == (1,)
    assert numpy.allclose(result.expand(), [other])
    tests.data.vectors.append(result)


@tests.parameterized
def test_vector_from_scalar(other):
    result = tensors.Vector(other)
    assert result.length == 1
    assert result.shape == (1,)
    assert numpy.allclose(result.expand(), [other.expand()])
    tests.data.vectors.append(result)


@tests.parameterized
def test_vector_from_vector(other):
    result = tensors.Vector(other)
    assert result.length == other.length
    assert result.shape == other.shape
    assert numpy.allclose(result.expand(), other.expand())


@tests.parameterized
@nose.tools.raises(TypeError)
def test_vector_fail(other):
    tensors.Vector(other)


@tests.parameterized
def test_matrix_from_number(other):
    result = tensors.Matrix(other)
    assert result.height == result.width == 1
    assert result.shape == (1, 1)
    assert numpy.allclose(result.expand(), [[other]])
    tests.data.matrices.append(result)


@tests.parameterized
def test_matrix_from_scalar(other):
    result = tensors.Matrix(other)
    assert result.height == result.width == 1
    assert result.shape == (1, 1)
    assert numpy.allclose(result.expand(), [[other.expand()]])
    tests.data.matrices.append(result)


@tests.parameterized
def test_matrix_from_vector(other):
    result = tensors.Matrix(other)
    assert result.height == other.length
    assert result.width == 1
    assert result.shape == (other.length, 1)
    assert numpy.allclose(result.expand(), other.expand().reshape(result.shape))
    tests.data.matrices.append(result)


@tests.parameterized
def test_matrix_from_matrix(other):
    result = tensors.Matrix(other)
    assert result.height == other.height
    assert result.width == other.width
    assert result.shape == other.shape
    assert numpy.allclose(result.expand(), other.expand())


@tests.parameterized
@nose.tools.raises(TypeError)
def test_matrix_fail(other):
    tensors.Matrix(other)


def generate_tests():
    numbers = tests.data.numbers[:]
    scalars = tests.data.scalars[:]
    vectors = tests.data.vectors[:]
    matrices = tests.data.matrices[:]
    yield from test_scalar_from_number(numbers)
    yield from test_scalar_from_scalar(scalars)
    yield from test_scalar_fail(vectors)
    yield from test_scalar_fail(matrices)
    yield from test_scalar_fail([None, "string"])
    yield from test_vector_from_number(numbers)
    yield from test_vector_from_scalar(scalars)
    yield from test_vector_from_vector(vectors)
    yield from test_vector_fail(matrices)
    yield from test_vector_fail([None, "string"])
    yield from test_matrix_from_number(numbers)
    yield from test_matrix_from_scalar(scalars)
    yield from test_matrix_from_vector(vectors)
    yield from test_matrix_from_matrix(matrices)
    yield from test_matrix_fail([None, "string"])
