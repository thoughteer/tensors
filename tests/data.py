import numpy


valid_sizes = [1, 2, 4, 8]
invalid_sizes = [-1, 0]
valid_accuracies = [1e-3, 1e-6]
invalid_accuracies = [-1e-3, -1e-6]
arrays = [numpy.arange(s + 1) for s in range(16)]
numbers = [0, 2e1, -3.14]
scalars = []
vectors = []
matrices = []
