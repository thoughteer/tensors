import nose.tools
import numpy

import tensors
import tests
import tests.data


def dot(left, right):
    return left.dot(right)


@tests.parameterized
def test_vector_dot(left, right):
    if left.length != right.length:
        nose.tools.assert_raises(AssertionError, dot, left, right)
        return
    result = dot(left, right)
    assert numpy.isclose(result, left.expand().dot(right.expand()))


@tests.parameterized
def test_vector_replicate(vector, count):
    result = vector.replicate(count)
    assert result.height == vector.length
    assert result.width >= count
    assert result.shape == (result.height, result.width)
    expected = numpy.repeat(vector.expand()[:, None], result.width, 1)
    assert numpy.allclose(result.expand(), expected)
    tests.data.matrices.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_vector_replicate_fail(vector, count):
    vector.replicate(count)


@tests.parameterized
def test_vector_times(left, right):
    result = left.times(right)
    assert result.height == left.length
    assert result.width == right.length
    assert result.shape == (result.height, result.width)
    expected = numpy.outer(left.expand(), right.expand())
    assert numpy.allclose(result.expand(), expected)
    tests.data.matrices.append(result)


def generate_tests():
    vectors = tests.data.vectors[:]
    yield from test_vector_dot(vectors, vectors)
    yield from test_vector_replicate(vectors, tests.data.valid_sizes)
    yield from test_vector_replicate_fail(vectors, tests.data.invalid_sizes)
    yield from test_vector_times(vectors, vectors)
