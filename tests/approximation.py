import inspect

import nose.tools
import numpy

import tensors.tools
import tests
import tests.data


def sine(x):
    return numpy.sin(x)


def hypotenuse(x, y):
    return numpy.sqrt(x**2 + y**2)


def test_functions():
    return (sine, hypotenuse)


@tests.parameterized
def test_approximation(*args, function=None, accuracy=None, collection=None):
    if any(a.shape != args[0].shape for a in args):
        nose.tools.assert_raises(AssertionError, function, *args)
        return
    try:
        approximation = function(*args)
    except RuntimeError:
        return
    assert approximation.shape == args[0].shape
    reference = function(*(a.expand().flatten() for a in args))
    error = numpy.linalg.norm(approximation.expand().flatten() - reference)
    assert error <= accuracy * numpy.linalg.norm(reference)
    collection.append(approximation)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_approximation_fail(accuracy):
    tensors.tools.tensorize(sine, accuracy)


def test_accuracy(accuracy, data):
    for function in test_functions():
        tensorized_function = tensors.tools.tensorize(function, accuracy)
        argcount = len(inspect.getargspec(function)[0])
        for argset, collection in data:
            yield from test_approximation(*([argset] * argcount),
                function=tensorized_function,
                accuracy=accuracy,
                collection=collection)


def generate_tests():
    collections = (tests.data.scalars, tests.data.vectors, tests.data.matrices)
    data = [(c[:], c) for c in collections]
    yield from test_approximation_fail(tests.data.invalid_accuracies)
    for accuracy in tests.data.valid_accuracies:
        yield from test_accuracy(accuracy, data)
