import nose.tools
import numpy

import tests
import tests.data


def dot(left, right):
    return left.dot(right)


@tests.parameterized
def test_matrix_dot_matrix(left, right):
    if left.width != right.height:
        nose.tools.assert_raises(AssertionError, dot, left, right)
        return
    result = dot(left, right)
    assert result.height == left.height
    assert result.width == right.width
    assert result.shape == (result.height, result.width)
    assert numpy.allclose(result.expand(), left.expand().dot(right.expand()))
    tests.data.matrices.append(result)


@tests.parameterized
def test_matrix_dot_vector(left, right):
    if left.width != right.length:
        nose.tools.assert_raises(AssertionError, dot, left, right)
        return
    result = dot(left, right)
    assert result.length == left.height
    assert result.shape == (result.length,)
    assert numpy.allclose(result.expand(), left.expand().dot(right.expand()))
    tests.data.vectors.append(result)


@tests.parameterized
def test_matrix_dot_array(left, right):
    if left.width != right.size:
        nose.tools.assert_raises(AssertionError, dot, left, right)
        return
    result = dot(left, right)
    assert result.size == left.height
    assert numpy.allclose(result, left.expand().dot(right))


@tests.parameterized
def test_matrix_times(left, right):
    result = left.times(right)
    assert result.height == left.height * right.height
    assert result.width == left.width * right.width
    assert result.shape == (result.height, result.width)
    expected = numpy.kron(left.expand(), right.expand())
    assert numpy.allclose(result.expand(), expected)
    tests.data.matrices.append(result)


@tests.parameterized
def test_matrix_transpose(matrix):
    result = matrix.transpose()
    assert result.height == matrix.width
    assert result.width == matrix.height
    assert result.shape == (result.height, result.width)
    assert numpy.allclose(result.expand(), matrix.expand().transpose())
    tests.data.matrices.append(result)


def generate_tests():
    vectors = tests.data.vectors[:]
    matrices = tests.data.matrices[:]
    yield from test_matrix_dot_matrix(matrices, matrices)
    yield from test_matrix_dot_vector(matrices, vectors)
    yield from test_matrix_dot_array(matrices, tests.data.arrays)
    yield from test_matrix_times(matrices, matrices)
    yield from test_matrix_transpose(matrices)
