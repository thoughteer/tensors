import nose.tools
import numpy

import tensors
import tests
import tests.data


@tests.deferred
def test_scalar_ones():
    result = tensors.Scalar.ones()
    assert result.shape == ()
    assert numpy.allclose(result.expand(), 1)
    tests.data.scalars.append(result)


@tests.parameterized
def test_vector_ones(length):
    result = tensors.Vector.ones(length)
    assert result.length >= length
    assert result.shape == (result.length,)
    assert numpy.allclose(result.expand(), numpy.ones(result.shape))
    tests.data.vectors.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_vector_ones_fail(length):
    tensors.Vector.ones(length)


@tests.parameterized
def test_matrix_ones(height, width):
    result = tensors.Matrix.ones(height, width)
    assert result.height >= height
    assert result.width >= width
    assert result.shape == (result.height, result.width)
    assert numpy.allclose(result.expand(), numpy.ones(result.shape))
    tests.data.matrices.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_matrix_ones_fail(height, width):
    tensors.Matrix.ones(height, width)


def test_tensor_ones():
    yield test_scalar_ones()
    yield from test_vector_ones(tests.data.valid_sizes)
    yield from test_vector_ones_fail(tests.data.invalid_sizes)
    yield from test_matrix_ones(tests.data.valid_sizes, tests.data.valid_sizes)
    yield from test_matrix_ones_fail(tests.data.valid_sizes,
        tests.data.invalid_sizes)
    yield from test_matrix_ones_fail(tests.data.invalid_sizes,
        tests.data.valid_sizes)
    yield from test_matrix_ones_fail(tests.data.invalid_sizes,
        tests.data.invalid_sizes)


@tests.deferred
def test_scalar_zeros():
    result = tensors.Scalar.zeros()
    assert result.shape == ()
    assert numpy.allclose(result.expand(), 0)
    tests.data.scalars.append(result)


@tests.parameterized
def test_vector_zeros(length):
    result = tensors.Vector.zeros(length)
    assert result.length >= length
    assert result.shape == (result.length,)
    assert numpy.allclose(result.expand(), numpy.zeros(result.shape))
    tests.data.vectors.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_vector_zeros_fail(length):
    tensors.Vector.zeros(length)


@tests.parameterized
def test_matrix_zeros(height, width):
    result = tensors.Matrix.zeros(height, width)
    assert result.height >= height
    assert result.width >= width
    assert result.shape == (result.height, result.width)
    assert numpy.allclose(result.expand(), numpy.zeros(result.shape))
    tests.data.matrices.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_matrix_zeros_fail(height, width):
    tensors.Matrix.zeros(height, width)


def test_tensor_zeros():
    yield test_scalar_zeros()
    yield from test_vector_zeros(tests.data.valid_sizes)
    yield from test_vector_zeros_fail(tests.data.invalid_sizes)
    yield from test_matrix_zeros(tests.data.valid_sizes, tests.data.valid_sizes)
    yield from test_matrix_zeros_fail(tests.data.valid_sizes,
        tests.data.invalid_sizes)
    yield from test_matrix_zeros_fail(tests.data.invalid_sizes,
        tests.data.valid_sizes)
    yield from test_matrix_zeros_fail(tests.data.invalid_sizes,
        tests.data.invalid_sizes)


@tests.parameterized
def test_vector_range(length):
    result = tensors.Vector.range(length)
    assert result.length >= length
    assert result.shape == (result.length,)
    assert numpy.allclose(result.expand(), numpy.arange(result.length))
    tests.data.vectors.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_vector_range_fail(length):
    tensors.Vector.range(length)


@tests.parameterized
def test_vector_step(length, origin):
    result = tensors.Vector.step(length, origin)
    assert result.length >= length
    assert result.shape == (result.length,)
    expected = (numpy.arange(result.length) >= origin).astype(float)
    assert numpy.allclose(result.expand(), expected)
    tests.data.vectors.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_vector_step_fail(length, origin):
    tensors.Vector.step(length, origin)


@tests.parameterized
def test_vector_delta(length, origin):
    result = tensors.Vector.delta(length, origin)
    assert result.length >= length
    assert result.shape == (result.length,)
    expected = (numpy.arange(result.length) == origin).astype(float)
    assert numpy.allclose(result.expand(), expected)
    tests.data.vectors.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_vector_delta_fail(length, origin):
    tensors.Vector.delta(length, origin)


@tests.parameterized
def test_matrix_identity(order):
    result = tensors.Matrix.identity(order)
    assert result.height == result.width >= order
    assert result.shape == (result.height, result.width)
    assert numpy.allclose(result.expand(), numpy.eye(result.height))
    tests.data.matrices.append(result)


@tests.parameterized
@nose.tools.raises(AssertionError)
def test_matrix_identity_fail(order):
    tensors.Matrix.identity(order)


def generate_tests():
    yield from test_tensor_ones()
    yield from test_tensor_zeros()
    yield from test_vector_range(tests.data.valid_sizes)
    yield from test_vector_range_fail(tests.data.invalid_sizes)
    yield from test_vector_step(tests.data.valid_sizes, [-1, 0, 1, 2, 100])
    yield from test_vector_step_fail(tests.data.invalid_sizes, [-1, 0, 1, 2, 100])
    yield from test_vector_delta(tests.data.valid_sizes, [-1, 0, 1, 2, 100])
    yield from test_vector_delta_fail(tests.data.invalid_sizes, [-1, 0, 1, 2, 100])
    yield from test_matrix_identity(tests.data.valid_sizes)
    yield from test_matrix_identity_fail(tests.data.invalid_sizes)
